#include <iostream>
#include <vector>
#include <numeric>

using namespace std;


void output_result(const vector<int>& numbers, int sum, int product) {
    cout << "numbers: ";
    for (auto n : numbers) {
        cout << n << " ";
    }
    cout << "sum: " << sum << " product: " << product << endl;
}


void find(int number_of_loops, int loop_until, const vector<int>& outside_numbers) {
    for (int i = 1; i < loop_until; ++i) {
        vector<int> cloned_outside_numbers(outside_numbers);
        cloned_outside_numbers.push_back(i);
        if (number_of_loops == 1) {
            int sum = accumulate(cloned_outside_numbers.begin(), cloned_outside_numbers.end(), 0);
            int product = accumulate(cloned_outside_numbers.begin(), cloned_outside_numbers.end(), 1, multiplies<>());
            if (sum == product) output_result(cloned_outside_numbers, sum, product);
        } else {
            find(number_of_loops-1, loop_until, cloned_outside_numbers);
        }
    }
}


int main() {
    find(24, 100, vector<int>());
    return 0;
}
